import os

INFRA_LOC = os.path.expanduser("~/tmp/experiments/ere-infra")
EXPORTERS_DIR = "exporters"
THEMES_DIR = "themes"
ERE_REPOS_DIR = "ere_repos"
ERE_TAR_DIR   = "ere"
BUILD_DIR     = "build"
CODE_DIR      = "code"
TAR_FILE      = "ere-%s.tar.gz"
PUBLISH_LOC   = "root@exp.vlabs.ac.in:/var/www/html/ere"

exporters = [{"org-exporter-9": {"url": "https://gitlab.com/vlead-projects/experiments/ere/publish/org-exporter-9.git",
                                 "branch": "master",
                                 "make_required": 0}}]


basic_themes = [{"default": {"url": "https://gitlab.com/vlead-projects/html-themes/default.git",
                            "branch": "master",
                            "make_required": 0}},
                {"readtheorg": {"url": "https://gitlab.com/vlead-projects/html-themes/readtheorg.git",
                                "branch": "master",
                                "make_required": 0}},
                {"vlead": {"url": "https://gitlab.com/vlead-projects/html-themes/vlead.git",
                           "branch": "master",
                           "make_required": 0}}]

ere_repos = [{"loader": {"url": "https://gitlab.com/vlead-projects/experiments/ere/loader.git",
                       "branch": "master",
                       "make_required": 1}},
              {"apis": {"url": "https://gitlab.com/vlead-projects/experiments/ere/apis.git",
                        "branch": "master",
                        "make_required": 1}},
              {"renderers": {"url": "https://gitlab.com/vlead-projects/experiments/ere/renderers.git",
                             "branch": "master",
                             "make_required": 1}},
              {"styles": {"url": "https://gitlab.com/vlead-projects/experiments/ere/styles.git",
                             "branch": "master",
                             "make_required": 1}}
            ]

org_modes = [{"org-9": "https://orgmode.org/org-9.1.13.tar.gz"}]
