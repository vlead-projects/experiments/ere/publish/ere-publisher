import importlib
import sys

from utils import *
from pub_config import *

def parse_args(*args):
    arg_dict = {}
    pairs = args[0]
    for pair in pairs:
        (key, val) = pair.split('=')
        arg_dict[key] = val

    return arg_dict

def clone_basic_themes():
    for theme in basic_themes:
        key = theme.keys()[0]
        fetch_and_build(THEMES_DIR, theme[key])

def get_version():
    module = check_module()

    if module is not None:
        version = module.version
    else:
        version = "master"

    return version

def get_ere_repo_version(repo):
    module = check_module()

    if module is not None:
        print "repo = %s" % repo
        print "ere versions = %s" % module.ere_repo_versions
        key_value = filter(lambda x: x.keys()[0] == repo, module.ere_repo_versions)
        print "key_value = %s" % key_value
        version = key_value[0].values()[0]
        print "version = %s" % version
    else:
        version = "master"

    return version

def clone_ere_repos():
    # version = get_version()
    for repo in ere_repos:
        print "repo = %s" % repo
        key = repo.keys()[0]
        print "key = %s" % key
        version = get_ere_repo_version(key);
        fetch_and_build(ERE_REPOS_DIR, repo[key], version)

def build_ere():
    version = get_version()
    ere_build(version)

def publish_ere():
    version = get_version()
    print version
    ere_publish(version)

def clone_exporters():
    for exporter in exporters:
        key = exporter.keys()[0]
        fetch_and_build(EXPORTERS_DIR, exporter[key])

def wget_orgs():
    for org_mode in org_modes:
        key = org_mode.keys()[0]
        wget_and_untar(key, org_mode[key])

def check_module():
    module = None
    try:
        module = importlib.import_module("make_config")
    except ImportError:
        print "module make_config not found"

    return module

def build():
    module = check_module()

    if module is not None:
        theme = module.theme
        exporter = module.exporter
    else:
        theme = "default"
        exporter = "org-exporter-9"

    copy_exporter(exporter)
    link_orgs(org_modes)
    copy_theme(theme)


def clean():
    clean_local(org_modes)

def infra_clean():
    clean_infra()

def run_infra():
    cmd = "cd %s; python -m SimpleHTTPServer 8002" % INFRA_LOC
    execute_command(cmd);


if __name__ == '__main__':
    arguments = parse_args(sys.argv[1:])
    target = arguments['target']
    if target == "clone-exporters":
        clone_exporters()
    elif target == "clone-basic-themes":
        clone_basic_themes()
    elif target == "clone-ere-repos":
        clone_ere_repos()
    elif target == "wget-orgs":
        wget_orgs()
    elif target == "build":
        build()
    elif target == "build-ere":
        build_ere()
    elif target == "publish-ere":
        publish_ere()
    elif target == "clean":
        clean()
    elif target == "clean-infra":
        infra_clean()
    elif target == "run-infra":
        run_infra()
