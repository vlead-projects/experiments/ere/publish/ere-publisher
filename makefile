clone-basic-infra: wget-orgs clone-exporters clone-basic-themes
	echo "infrastructure put in place"

clone-exp-infra: clone-basic-infra clone-ere-repos
	echo "ere infrastructure put in place"

clone-basic-themes:
	(export PYTHONPATH=`pwd`; python ere-publisher/targets.py target=clone-basic-themes)

clone-exporters:
	(export PYTHONPATH=`pwd`; python ere-publisher/targets.py target=clone-exporters)

wget-orgs:
	(export PYTHONPATH=`pwd`; python ere-publisher/targets.py target=wget-orgs)

clone-ere-repos:
	(export PYTHONPATH=`pwd`; python ere-publisher/targets.py target=clone-ere-repos)

build-ere:
	(export PYTHONPATH=`pwd`; python ere-publisher/targets.py target=build-ere)

publish-ere:
	(export PYTHONPATH=`pwd`; python ere-publisher/targets.py target=publish-ere)

clean-build:
	rm -rf build

build: clean-build
	(export PYTHONPATH=`pwd`; python ere-publisher/targets.py target=build)
	emacs --script elisp/publish.el

clean:
	(export PYTHONPATH=`pwd`; python ere-publisher/targets.py target=clean)

clean-infra:
	(export PYTHONPATH=`pwd`; python ere-publisher/targets.py target=clean-infra)

run-infra:
	(export PYTHONPATH=`pwd`; python ere-publisher/targets.py target=run-infra)
